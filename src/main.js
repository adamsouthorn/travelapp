import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import routes from '../router/router'
import VueMeta from 'vue-meta'
import store from './store'

Vue.use(VueMeta)
Vue.use(VueRouter)
// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
}).$mount('#app')
