export const STORE_TRIPS = 'storeTrips'
export const UPDATE_TRIP = 'updateTrip'
export const ADD_TRIP = 'addTrip'
export const DELETE_TRIP = 'deleteTrip'
