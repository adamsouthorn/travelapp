export const getTrip = (state) => uid => state.trips.find(trip => trip.uid === uid)
export const getTrips = (state) => {
  return state.trips
}
