export const storeTrips = (state, trips) => {
  state.trips = trips
}

export const updateTrip = (state, trip) => {
  const newState = state.trips.map(item => {
    return item.uid === trip.uid ? trip : item
  })
  state.trips = newState
}

export const addTrip = (state, trip) => {
  state.trips.push(trip)
}

export const deleteTrip = (state, uid) => {
  const filteredTrips = state.trips.filter(item => item.uid !== uid)
  state.trips = filteredTrips
}
