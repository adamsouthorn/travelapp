import { tripsRef, tripCategoryRef } from 'src/helpers/firebaseRefs.js'
import { getTrip, getData } from 'src/helpers/store.js'
import { STORE_TRIPS, ADD_TRIP, UPDATE_TRIP, DELETE_TRIP } from './mutation.types'
import { openDB, deleteDB } from 'idb'

export const setTrips = async ({ state, commit }, reset) => {
  if (reset) {
    commit(STORE_TRIPS, null)
    deleteDB('meta')
  } else if (!state.trips) {
    await tripsRef().get()
      .then(querySnapshot => {
        const trips = getData(querySnapshot)
        const tripsWithMeta = trips.map(trip => {
          trip.meta = {}
          return trip
        })
        commit(STORE_TRIPS, tripsWithMeta)
      })
  }
}

export const addTrip = ({ commit }, { trip }) => commit(ADD_TRIP, trip)
export const updateTrip = ({ commit }, { trip }) => commit(UPDATE_TRIP, trip)
export const deleteTrip = ({ commit }, { uid }) => commit(DELETE_TRIP, uid)

export const setMeta = async ({ state, commit }, { uid, meta }) => {
  const db = await openDB('meta', 1, {
    upgrade (db) {
      db.createObjectStore('metaStore')
    }
  })

  const trip = getTrip(state, uid)
  let metaFromCache = false
  if ((trip && !trip.meta[meta])) {
    const cache = db.transaction('metaStore', 'readwrite').objectStore('metaStore')
    if (!window.navigator.onLine) {
      const cachedMeta = await cache.get(`${trip.uid}/${meta}`)
      if (cachedMeta) {
        metaFromCache = true
      }
      trip.meta[meta] = cachedMeta
      commit(UPDATE_TRIP, trip)
    }
    if (!metaFromCache) {
      await tripCategoryRef(uid, meta).get()
        .then(async querySnapshot => {
          trip.meta[meta] = getData(querySnapshot)
          const cache = db.transaction('metaStore', 'readwrite').objectStore('metaStore')
          await cache.put(trip.meta[meta], `${trip.uid}/${meta}`)
          commit(UPDATE_TRIP, trip)
        })
    }
  }
}

export const addMeta = ({ state, commit }, { uid, meta, newMeta }) => {
  const trip = getTrip(state, uid)
  trip.meta[meta].push(newMeta)
  commit(UPDATE_TRIP, trip)
}

export const updateMeta = ({ state, commit }, { uid, meta, updatedMeta, updatedMetaUid }) => {
  const trip = getTrip(state, uid)
  const newState = trip.meta[meta].map(item => {
    return item.uid === updatedMetaUid ? updatedMeta : item
  })
  trip.meta[meta] = newState
  commit(UPDATE_TRIP, trip)
}

export const deleteMeta = ({ state, commit }, { uid, meta, deletedMeta }) => {
  const trip = getTrip(state, uid)
  trip.meta[meta] = trip.meta[meta].filter(item => item.uid !== deletedMeta.uid)
  commit(UPDATE_TRIP, trip)
}
