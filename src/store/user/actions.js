import { userRef } from 'src/helpers/firebaseRefs.js'
import { STORE_USER_DETAILS, STORE_USER_ID } from './mutation.types'

export const setUserId = ({ commit }, { userId }) => commit(STORE_USER_ID, userId)

export const setUserDetails = async ({ commit }) => {
  await userRef().get()
    .then(user => {
      const userDetails = user.data()
      commit(STORE_USER_DETAILS, userDetails)
    })
}

export const updateUserDetails = ({ commit }, { userDetails }) => commit(STORE_USER_DETAILS, userDetails)
