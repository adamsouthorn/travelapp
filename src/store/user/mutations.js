export const storeUserId = (state, userId) => {
  state.userId = userId
}
export const storeUserDetails = (state, user) => {
  state.userDetails = user
}
