export const SET_USER_ID = 'user/setUserId'
export const SET_USER_DETAILS = 'user/setUserDetails'
export const UPDATE_USER_DETAILS = 'user/updateUserDetails'
