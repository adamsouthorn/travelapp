import Vue from 'vue'
import firebase from 'firebase/app'
import 'firebase/analytics'
import 'firebase/auth'
import 'firebase/firestore'
import { config } from 'src/config'

firebase.initializeApp(config.firebase)

firebase.firestore().enablePersistence({ synchronizeTabs: true }).catch(err => {
  if (err.code === 'failed-precondition') {
    console.log('App open in multiple tabs open. Close all but one to continue')
  } else if (err.code === 'unimplemented') {
    console.log('The current browser does not support all of the features required to enable persistence')
  }
})

Vue.prototype.$firebase = firebase
