import store from 'src/store'
import { GET_USER_DETAILS } from 'src/store/user/getter.types'

const userDetails = store.getters[GET_USER_DETAILS]
export const getCurrency = (userDetails && userDetails.currency.value) || '£'
export const getCurrencyIso = (userDetails && userDetails.currency.iso) || 'GBP'
