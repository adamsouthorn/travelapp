const Flight = { name: 'Flight', icon: 'flight' }
const Train = { name: 'Train', icon: 'train' }
const Boat = { name: 'Boat', icon: 'directions_boat' }
const Bus = { name: 'Bus', icon: 'directions_bus' }
const Car = { name: 'Car', icon: 'directions_car' }
const Bike = { name: 'Bike', icon: 'directions_bike' }

const TransportTypes = [
  Flight,
  Train,
  Boat,
  Bus,
  Car,
  Bike
]

export default TransportTypes
