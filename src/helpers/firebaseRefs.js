import firebase from 'firebase/app'
import 'firebase/firebase-firestore'
import 'firebase/firebase-analytics'
import store from 'src/store'
import { GET_USER_ID } from 'src/store/user/getter.types'

export const userRef = () => firebase.firestore().collection('users').doc(store.getters[GET_USER_ID])

export const tripsRef = () => userRef().collection('trips')

export const tripRef = (tripId) => tripsRef().doc(tripId)

export const tripCategoryRef = (tripId, collection) => {
  return tripRef(tripId).collection(collection)
}

export const tripCategoryItemRef = (tripId, collection, itemId) => tripCategoryRef(tripId, collection).doc(itemId)

export const firebaseTimeStamp = firebase.firestore.FieldValue.serverTimestamp()

export const logRef = () => firebase.firestore().collection('logs')

export const analytics = () => firebase.analytics()
