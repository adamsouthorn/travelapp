import metaTypes from 'src/helpers/metaTypes.js'

const getAmount = (collection) => {
  return !collection ? null : collection
    .map(value => parseFloat(value.price))
    .reduce((a, b) => {
      return (a || 0) + (b || 0)
    }, 0)
}

const getAmounts = (meta) => {
  const amounts = {}
  amounts.accommodation = getAmount(meta.accommodation)
  amounts.transport = getAmount(meta.transport)
  amounts.activities = getAmount(meta.activities)
  amounts.food = getAmount(meta.food)
  amounts.other = getAmount(meta.other)
  return amounts
}

const setMetaOverview = (meta) => {
  const overview = { ...meta }
  if (overview) {
    const transport = overview.transport ? overview.transport.map(item => {
      item.type = metaTypes.Transport
      item.order = item.timestamp
      return item
    }) : []
    const places = overview.places ? overview.places.map(item => {
      item.type = metaTypes.Places
      item.order = item.timestamp + 1
      return item
    }) : []
    const accommodation = overview.accommodation ? overview.accommodation.map(item => {
      item.type = metaTypes.Accommodation
      item.order = item.timestamp + 2
      return item
    }) : []
    const activities = overview.activities ? overview.activities.map(item => {
      item.type = metaTypes.Activities
      item.order = item.timestamp + 3
      return item
    }) : []
    const metaCompiled = [
      ...transport,
      ...activities,
      ...accommodation,
      ...places
    ]
    return metaCompiled.sort((a, b) => a.order - b.order)
  }
}

const setMap = (meta) => {
  const coords = meta.map(item => {
    const coords = item.lat && {
      lat: item.lat,
      lng: item.lng,
      name: item.name
    }
    const transportCoords = []
    if (item.from && item.from.lat) {
      transportCoords.push({
        lat: item.from.lat,
        lng: item.from.lng,
        name: item.name
      })
    }
    if (item.to && item.to.lat) {
      transportCoords.push({
        lat: item.to.lat,
        lng: item.to.lng
      })
    }
    return coords || transportCoords
  })
  return [coords.filter(item => item !== undefined).flat()]
}

export { setMetaOverview, setMap, getAmounts }
