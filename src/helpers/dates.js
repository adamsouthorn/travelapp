export const qDate = (date) => {
  date = date.split('/')
  return `${date[2]}/${date[1]}/${date[0]}`
}

export const timestamp = (date) => {
  date = date.split('/')
  const parsedDate = new Date(`${date[1]}/${date[0]}/${date[2]}`)
  return parsedDate.getTime()
}

export const msToDays = (ms) => {
  return ms / (60 * 60 * 24 * 1000)
}
