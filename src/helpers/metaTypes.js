const Places = 'places'
const Accommodation = 'accommodation'
const Transport = 'transport'
const Activities = 'activities'
const Food = 'food'
const Other = 'other'

const MetaTypes = {
  Places,
  Accommodation,
  Transport,
  Activities,
  Food,
  Other
}

export default MetaTypes
