import { logRef, firebaseTimeStamp } from 'src/helpers/firebaseRefs'
import store from 'src/store'
import { GET_USER_ID } from 'src/store/user/getter.types'

export default (error) => {
  try {
    const userId = store.getters[GET_USER_ID] || null
    const errorCode = error.code || null
    const errorMessage = error.message || null
    const email = error.email || null // only sent on login
    const credential = error.credential || null // only sent on login
    logRef().add({
      type: 'error',
      userId,
      errorCode,
      email,
      credential,
      errorMessage: errorMessage || error,
      timestamp: firebaseTimeStamp
    })
  } catch (error) {
    console.log(error)
  }
}
