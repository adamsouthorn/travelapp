import { config } from 'src/config'
import axios from 'axios'
import logError from 'src/helpers/error.js'

const endpoint = 'https://api.openweathermap.org/data/2.5/weather'

const getWeather = async (lat, lng) => {
  try {
    const response = await axios.get(`${endpoint}?appid=${config.api.weather}&lat=${lat}&lon=${lng}`)
    if (response.data && response.data.weather) {
      return response.data.weather[0]
    }
  } catch (error) {
    logError(error)
  }
}

export { getWeather }
