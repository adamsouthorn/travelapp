import { config } from 'src/config'
import axios from 'axios'
import { qDate } from 'src/helpers/dates'

const endpoint = 'http://api.aviationstack.com/v1/flights'

const getFlight = (date) => {
  date = '03/04/2020'
  const formattedDate = qDate(date).replace(/\//g, '-')
  axios.get(`${endpoint}?access_key=${config.api.aviation}&flight_iata=BA213`)
    .then(response => {
      const data = response.data.data
      data.forEach(flight => {
        if (flight.flight_date === formattedDate) {
          console.log(flight)
        }
      })
    })
    .catch(error => {
      console.log(error)
    })
}

export { getFlight }
