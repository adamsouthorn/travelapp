import axios from 'axios'
import logError from 'src/helpers/error.js'

const endpoint = 'https://api.exchangeratesapi.io/latest'

const getExchangeValue = async (from, to) => {
  try {
    const response = await axios.get(`${endpoint}?base=${from}&symbols=${to}`)
    if (response.data && response.data.rates && response.data.rates[to]) {
      return response.data.rates[to]
    }
    return 0
  } catch (error) {
    logError(error)
  }
}

const currencies = [
  {
    label: 'British Pound',
    value: '£',
    iso: 'GBP'
  },
  {
    label: 'United States Dollar',
    value: '$',
    iso: 'USD'
  },
  {
    label: 'Euro',
    value: '€',
    iso: 'EUR'
  },
  {
    label: 'Australian dollar',
    value: 'A$',
    iso: 'AUD'
  },
  {
    label: 'Canadian dollar',
    value: 'C$',
    iso: 'CAD'
  },
  {
    label: 'Bulgarian Lev',
    value: 'Лв',
    iso: 'BGN'
  },
  {
    label: 'Brazilian Real',
    value: 'R$',
    iso: 'BGN'
  },
  {
    label: 'Chinese Yuan Renminbi',
    value: '¥',
    iso: 'CNY'
  },
  {
    label: 'Croatian kuna',
    value: 'kn',
    iso: 'HRK'
  },
  {
    label: 'Czech koruna',
    value: 'Kč',
    iso: 'CZK'
  },
  {
    label: 'Danish krone',
    value: 'Kr.',
    iso: 'DKK'
  },
  {
    label: 'Hong Kong dollar',
    value: 'HK$',
    iso: 'HKD'
  },
  {
    label: 'Hungarian forint',
    value: 'Ft',
    iso: 'HUF'
  },
  {
    label: 'Icelandic króna',
    value: 'kr (ISK)',
    iso: 'ISK'
  },
  {
    label: 'Indian rupee',
    value: '₹',
    iso: 'INR'
  },
  {
    label: 'Indonesian rupiah',
    value: 'Rp',
    iso: 'IDR'
  },
  {
    label: 'Israeli Shekel',
    value: '₪',
    iso: 'ILS'
  },
  {
    label: 'Japanese yen',
    value: '¥',
    iso: 'JPY'
  },
  {
    label: 'Malaysian ringgit',
    value: 'RM',
    iso: 'MYR'
  },
  {
    label: 'Mexican peso',
    value: 'Mex$',
    iso: 'MXN'
  },
  {
    label: 'Norwegian krone',
    value: 'kr (NOK)',
    iso: 'NOK'
  },
  {
    label: 'New Zealand dollar',
    value: 'NZ$',
    iso: 'NZD'
  },
  {
    label: 'Philippine peso',
    value: '₱',
    iso: 'PHP'
  },
  {
    label: 'Polish złoty',
    value: 'zł',
    iso: 'PLN'
  },
  {
    label: 'Romanian leu',
    value: 'lei',
    iso: 'RON'
  },
  {
    label: 'Russian ruble',
    value: '₽',
    iso: 'RUB'
  },
  {
    label: 'Singapore dollar',
    value: 'S$',
    iso: 'SGD'
  },
  {
    label: 'South African rand',
    value: 'R',
    iso: 'ZAR'
  },
  {
    label: 'South Korean won',
    value: '₩',
    iso: 'KRW'
  },
  {
    label: 'Swedish krona',
    value: 'kr (SEK)',
    iso: 'SEK'
  },
  {
    label: 'Swiss Franc',
    value: 'R$',
    iso: 'CHf'
  },
  {
    label: 'Thai baht',
    value: '฿',
    iso: 'THB'
  },
  {
    label: 'Turkish lira',
    value: '₺',
    iso: 'TRY'
  }
]

export { currencies, getExchangeValue }
