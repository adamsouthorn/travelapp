export const getTrip = (state, uid) => {
  return state.trips.find(trip => trip.uid === uid)
}

export const getData = (querySnapshot) => {
  const array = []
  querySnapshot.forEach(doc => {
    const data = doc.data()
    data.uid = doc.id
    array.push(data)
  })
  return array
}
