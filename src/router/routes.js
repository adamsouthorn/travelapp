
import firebase from 'firebase/app'

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    beforeEnter: async (to, from, next) => {
      try {
        firebase.auth().onAuthStateChanged(user => {
          if (!user) {
            next('/login')
          } else {
            next()
          }
        })
      } catch (err) {
        next()
      }
    },
    meta: { requiresAuth: true },
    children: [
      { path: '', name: 'home', component: () => import('pages/Index.vue') },
      { path: 'add', name: 'add', component: () => import('pages/trip/Add.vue') },
      { path: 'profile', name: 'profile', component: () => import('pages/Profile.vue') },
      { path: 'trip/:uid', name: 'trip', component: () => import('pages/trip/Trip.vue') },
      { path: 'trip/:uid/edit', name: 'edit', component: () => import('pages/trip/Add.vue') },
      { path: 'trip/:uid/place/:uid2', name: 'editPlace', component: () => import('pages/places/Add.vue') },
      { path: 'trip/:uid/accommodation', name: 'accommodation', component: () => import('pages/accommodation/Accommodation.vue') },
      { path: 'trip/:uid/accommodation/:uid2', name: 'editAccommodation', component: () => import('pages/accommodation/Add.vue') },
      { path: 'trip/:uid/transport', name: 'transport', component: () => import('pages/transport/Transport.vue') },
      { path: 'trip/:uid/transport/:uid2', name: 'editTransport', component: () => import('pages/transport/Add.vue') },
      { path: 'trip/:uid/activities', name: 'activities', component: () => import('pages/activities/Activities.vue') },
      { path: 'trip/:uid/activities/:uid2', name: 'editActivities', component: () => import('pages/activities/Add.vue') },
      { path: 'trip/:uid/food', name: 'food', component: () => import('pages/food/Food.vue') },
      { path: 'trip/:uid/food/:uid2', name: 'editFood', component: () => import('pages/food/Add.vue') },
      { path: 'trip/:uid/other', name: 'other', component: () => import('pages/other/Other.vue') },
      { path: 'trip/:uid/other/:uid2', name: 'editOther', component: () => import('pages/other/Add.vue') }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/AuthLayout.vue'),
    beforeEnter: async (to, from, next) => {
      try {
        firebase.auth().onAuthStateChanged(user => {
          if (user) {
            next('/')
          } else {
            next()
          }
        })
      } catch (err) {
        next()
      }
    },
    children: [
      { path: '', name: 'login', component: () => import('pages/Login.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
